# WHAT:   server to control berryclip from within scratch
# AUTHOR: Tim Van den Bulcke
# DATE:   2013-03-20
# NOTE:   you need to enable the server in scratch by rightclicking 
#         on a sensor value (='waarde van een sensor' in Dutch) 
#         and enabling the external sensor connections
#         scratch will then send messages to 127.0.0.1:42001
# NOTE:   some of the variables are in Dutch        


import socket
import time
from array import array
import RPi.GPIO as GPIO
import re
import threading



# Tell GPIO library to use GPIO references
GPIO.setmode(GPIO.BCM)

berrypinsout= {'led1': 4, 'led2': 17, 'led3': 22, 'led4': 10, 'led5': 9,'led6': 11, 'zoemer': 8}
berrypinsin = {'knop': 7}
sleep_delay = 0.05 # time resolution for polling. Keep a balance between a good time resolution and CPU load here.

# Configure GPIO pins
for p in berrypinsout.values():
    GPIO.setup(p, GPIO.OUT)
    GPIO.output(p, False)
for p in berrypinsin.values():
    GPIO.setup(p, GPIO.IN)


# setup scratch connection
HOST = '127.0.0.1'
PORT = 42001
print("connecting to %s:%s ..." % (HOST,PORT))
scratchSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
scratchSock.connect((HOST,PORT))
print("connected!")


def send_scratch_command(str):
    """take a message string and make a scratch command 
       that can be sent over the socket.
       E.g. 'broadcast "Hello!"' is a message string
    """
    n = len(str)
    a = array('c')
    a.append(chr((n>>24) & 0xFF))
    a.append(chr((n>>16) & 0xFF))
    a.append(chr((n>> 8) & 0xFF))
    a.append(chr(n & 0xFF))
    scratchSock.send(a.tostring() + str)

def read_scratch_command(str):
    """reads a scratch command from the socket and creates a string"""
    vals = [ord(x) for x in str[0:4]]
    #for x in str[0:4]:
    #    print "-%i-," % ord(x),
    l = 0
    for (i,v) in enumerate(vals):
        l += v * 8**(3-i)
    #print "length %i - %s" % (l, str[4:(4+l)] )
    return( l, str[4:(4+l)], str[(4+l):] )

def scratch_broadcast(msg):
    """broadcast a string to scratch"""
    send_scratch_command('broadcast "%s"' % msg)

def scratch_sensor_update(sensor, value):
    """send a sensor-update to scratch"""
    send_scratch_command('sensor-update "%s" %i' %(sensor, value))

#scratch_broadcast("hello!")
#scratch_sensor_update("schuif", 13)

#def send_pulse_to_pin(pin, time1):
#        GPIO.output(pin, True)
#        time.sleep(time1)
#        GPIO.output(pin, False)



rxbcast    = re.compile('^(broadcast) "(.*)"$')
rxsensup   = re.compile('^(sensor-update) (.*)$')
rxsensupval= re.compile('"([^"]+)" ([^ ]+) *')
#rxcommand  = re.compile('^([sensor-update|broadcast]) "(.*)"( (.*))*$')
rxberry = re.compile('^berry (.*) ([on|off|aan|uit|0|1]*)')


# process the inputs from the berryclip 
# (in case of the berry clip, there is only one button so this could be written way simpler)
# this loop needs to run in a separate thread so both inputs and outputs are processed independently and simultaneously
class ProcessInputs(threading.Thread):
    def run(self):
        bLoop1 = True
        prevPinValue = {}
        for (name,pin) in berrypinsin.items():
            prevPinValue[pin] = 0
        while bLoop1:
            time.sleep(sleep_delay)
            for (name,pin) in berrypinsin.items():
                #print "checking %s-%s" % (name, pin)
                newValue = GPIO.input(pin)
                if newValue!=prevPinValue[pin]:
                    print "detected sensor change: %s-%s to %s" % (name, pin, newValue)
                    prevPinValue[pin] = newValue
                    scratch_sensor_update(name, newValue)
p = ProcessInputs()
p.start()




##### main loop ####
bLoop = True
while bLoop:
    time.sleep(sleep_delay)
    
    # process the signals from scratch:
    datarcv = scratchSock.recv(1024)
    data = datarcv
    if not datarcv:
        break
    #print "## MSG RECEIVED ##"
    #print "## len raw data = %i" % len(datarcv)
    #print "## raw data = -%s-" % datarcv
    #print("## data -%s-" % data)
    while len(data)>0:
        rslt = read_scratch_command(data)
        command = rslt[1]
        data = rslt[2]
        type = ''
        m = rxsensup.match(command)
        if m is None:
            m = rxbcast.match(command)
        if m is not None:
            g = m.groups()
            type = g[0]
            str = g[1]
        else:
            print 'received an invalid command: %s' % command
        #print("str -%s-" % str)
        if type=='broadcast':
            if str == 'berry close':
                print 'berry close - closing down script ...'
                bLoop = False
                break
            elif str.startswith('berry') and rxberry.match(str) is not None:
                g = rxberry.match(str).groups()
                print("berry command: -%s-%s-" % (g[0],g[1]))
                if g[0] in berrypinsout.keys():
                    pin = berrypinsout[g[0]]
                    #print g[0],'  ',pin
                    if g[1] in ['aan', 'on', '1']:
                        GPIO.output(pin, True)
                    elif g[1] in ['uit', 'off', '0']:
                        GPIO.output(pin, False)
            else:
                print("invalid broadcast message received: '%s'" % str)
        elif type=='sensor-update':
            print("signal sensor-update: -%s-" % (str))
            if str == 'close':
                print 'sensor close - closing down script ...'
                bLoop = False
                break
            else:
                m = rxsensupval.findall(str)
                if len(m)>0:
                    for (attr,val) in m:
                        if attr in berrypinsout.keys():
                            pin = berrypinsout[attr]
                            #print attr,' --> ',pin
                            if val in ['aan', 'on', '1']:
                                GPIO.output(pin, True)
                            elif val in ['uit', 'off', '0']:
                                GPIO.output(pin, False)
                            else:
                                print '   invalid sensor value received: %s-%s' % (attr,val)
                else:
                    print("invalid message received")


print("closing socket...")
scratchSock.close()
print("cleanup GPIO")
GPIO.cleanup()
print("finished.")
